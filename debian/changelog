fortunes-it (2.8-1) unstable; urgency=medium

  * New upstream release

 -- Salvo 'LtWorf' Tomaselli <ltworf@debian.org>  Tue, 01 Oct 2024 23:09:17 +0200

fortunes-it (2.7-1) unstable; urgency=medium

  * New upstream release

 -- Salvo 'LtWorf' Tomaselli <ltworf@debian.org>  Fri, 06 Sep 2024 07:35:54 +0200

fortunes-it (2.6-1) unstable; urgency=low

  * New upstream release

 -- Salvo 'LtWorf' Tomaselli <ltworf@debian.org>  Wed, 28 Aug 2024 15:45:05 +0200

fortunes-it (2.5-1) unstable; urgency=low

  * New upstream release
  * Update copyright file
  * Update description for fortunes-it-brianza
  * Add upstream/metadata file

 -- Salvo 'LtWorf' Tomaselli <ltworf@debian.org>  Sat, 22 Jun 2024 11:12:42 +0200

fortunes-it (2.4-2) unstable; urgency=low

  * Set Vcs-* fields in control

 -- Salvo 'LtWorf' Tomaselli <ltworf@debian.org>  Tue, 14 May 2024 13:51:01 +0200

fortunes-it (2.4-1) unstable; urgency=low

  * New upstream release (Closes: #1065031)

 -- Salvo 'LtWorf' Tomaselli <ltworf@debian.org>  Thu, 18 Apr 2024 21:38:27 +0200

fortunes-it (2.3-1) unstable; urgency=low

  * New upstream release

 -- Salvo 'LtWorf' Tomaselli <ltworf@debian.org>  Wed, 21 Feb 2024 00:35:14 +0100

fortunes-it (2.2-3) unstable; urgency=low

  * Non-deferred source only upload

 -- Salvo 'LtWorf' Tomaselli <ltworf@debian.org>  Tue, 30 Jan 2024 23:58:15 +0100

fortunes-it (2.2-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * source only upload to enable migration (Closes: #1061721)

 -- Paul Gevers <elbrus@debian.org>  Mon, 29 Jan 2024 07:24:47 +0100

fortunes-it (2.2-2) unstable; urgency=low

  * New binary packages to install the Sicilian fortunes

 -- Salvo 'LtWorf' Tomaselli <ltworf@debian.org>  Thu, 30 Nov 2023 11:43:25 +0100

fortunes-it (2.2-1) unstable; urgency=low

  * New upstream release
  * Drop version from fortune-mod dependency

 -- Salvo 'LtWorf' Tomaselli <ltworf@debian.org>  Thu, 30 Nov 2023 11:29:59 +0100

fortunes-it (2.1-1) unstable; urgency=low

  * New upstream release
  * Drop version from fortune-mod dependency

 -- Salvo 'LtWorf' Tomaselli <ltworf@debian.org>  Wed, 22 Nov 2023 19:40:41 +0100

fortunes-it (2.0-2) unstable; urgency=low

  * Source-only upload
  * Set homepage in control file
  * Set rules-requires-root to no
  * dep5 copyright file, and add myself to it
  * Bump Standards-Version: 4.6.2
  * Bump compat to 13

 -- Salvo 'LtWorf' Tomaselli <ltworf@debian.org>  Fri, 17 Nov 2023 03:29:31 +0100

fortunes-it (2.0-1) unstable; urgency=low

  * New upstream package
  * Set myself as maintainer (Closes: #1051200)
  * Add public key to verify the .orig file
  * Simplify rules file
  * Drop debian patches, accepted in the upstream
  * Point to the new, forked, upstream
  * Move proverbi della Brianza in a separate package
    since they aren't in Italian.

 -- Salvo 'LtWorf' Tomaselli <ltworf@debian.org>  Wed, 15 Nov 2023 19:54:26 +0100

fortunes-it (1.99-4.3) unstable; urgency=low

  * Non-maintainer upload.
  * Fix quotes. (Closes: #497203, #494407)
  * Remove proverbi della Brianza, since they aren't in Italian (Closes: #497367)

 -- Salvo 'LtWorf' Tomaselli <ltworf@debian.org>  Tue, 14 Nov 2023 18:26:44 +0100

fortunes-it (1.99-4.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Convert to source format 3.0. (Closes: #1007415)
  * Do not claim that Franklin was a president. (Closes: #1016115)

  [Raúl Benencia]
  * Fix typo in testi/paolotedeschi. (Closes: #788097)
  * Fix typo in testi/zuse. (Closes: #792271)
  * Fix typo in testi/luke. (Closes: #833062)

  [Vijeth T Aradhya]
  * Fix typo in testi/italia. (Closes: #787140)

 -- Bastian Germann <bage@debian.org>  Fri, 01 Sep 2023 17:03:45 +0200

fortunes-it (1.99-4.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix tpo in italian translation (Closes: #813924)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Fri, 02 Aug 2019 12:55:27 +0200

fortunes-it (1.99-4) unstable; urgency=low

  * Move offensive quotes to -off files. (Closes: #785024)
  * Fix typos in testi/leggi, testi/italia, testi/norm, testi/luttazzi,
    testi/luke. (Closes: #771368, #785493, #782040, #783534, #783967, #784805)
  * Bump Standards-Version.
  * Set debhelper compatibility level to 9.
  * Depend on ${misc:Depends} as this package uses debhelper.
  * Add missing debhelper targets: build-arch, build-indep.

 -- Emanuele Rocca <ema@debian.org>  Wed, 20 May 2015 06:41:14 +0200

fortunes-it (1.99-3) unstable; urgency=low

  * Fix misplaced comma in testi/formiche. (Closes: #442744)
  * Fix capitalisation of "Mark" in testi/norm. (Closes: #447374)
  * s/Aquari/Acquari in testi/italia. (Closes: #457117)
  * s/procastinazione/procrastinazione in testi/leggi.
  * Do not call dh_makeshlibs in debian/rules.
  * Added Build-Depends-Indep, debhelper alone left in Build-Depends, needed
    by the clean target.
  * Watch file added.
  * Standards-Version set to 3.8.0.

 -- Emanuele Rocca <ema@debian.org>  Fri, 08 Aug 2008 12:56:09 +0200

fortunes-it (1.99-2) unstable; urgency=medium

  * Urgency set to medium, closes a RC bug in testing.
  * binary-arch target added to debian/rules. (Closes: #395598)
  * binary-*: added dependency on the build target.
  * Build-Depends-Indep changed with Build-Depends, debhelper is needed by
    the clean target of debian/rules.
  * s/è/e`/ in debian/control, è is forbidden by RFC822.
  * Standards-Version set to 3.7.2

 -- Emanuele Rocca <ema@debian.org>  Sat, 28 Oct 2006 02:29:27 +0200

fortunes-it (1.99-1) unstable; urgency=low

  * Believe it or not, new upstream release! :)
    Thanks to Andrea 'Zuse' Balestrero for his great work
  * debian/rules rewritten to support multiple fortunes files; debhelper
    compatibility level set to 4
  * fortunes-it-off: README.Debian.offensive renamed in README.Debian
  * Updated debian/copyright
  * Updated Standards Version

 -- Emanuele Rocca <ema@debian.org>  Tue, 14 Dec 2004 00:34:29 +0100

fortunes-it (1.51-9) unstable; urgency=low

  * Fixed permissions of italia.dat and zozzital.dat (0664 --> 0644)
  * Maintainer field updated. Pasc, thanks for sponsoring!

 -- Emanuele Rocca <ema@debian.org>  Tue, 11 Nov 2003 21:37:51 +0100

fortunes-it (1.51-8) unstable; urgency=low

  * Added informations about the sources from which the fortunes were taken
    from. (Closes: #209583)
  * Sponsored by Pascal Hakim <pasc@debian.org>.

 -- Emanuele Rocca <emarocca@libero.it>  Wed, 10 Sep 2003 14:02:49 +0200

fortunes-it (1.51-7) unstable; urgency=low

  * Splitted in two different binary packages: fortunes-it contains
    normal cookies, fortunes-it-off contains offensive cookies.
  * Added utf-8 support.
  * Data files moved
    - fortunes-it: from /usr/share/games/fortunes to
      /usr/share/games/fortunes/it
    - fortunes-it-off: from /usr/share/games/fortunes/off to
      /usr/share/games/fortunes/off/it
  * Linda fix: Long descriptions contains short description.
  * Sponsored by Pascal Hakim <pasc@debian.org>.

 -- Emanuele Rocca <emarocca@libero.it>  Tue, 22 Jul 2003 20:04:58 +0200

fortunes-it (1.51-6) unstable; urgency=low

  * New maintainer.
  * Corrected spelling error in description: italian -> Italian.
    (Closes: #124633)
  * Added the possibility to reinstall offensive fortunes previously
    removed.
  * Lintian fixes:
  	dh_suidregister-is-obsolete
    	postinst-should-not-set-usr-doc-link
  * Fixed spelling error in debian/postinst:
    	is installed with the set of... -> is installed with a set of...
  * Sponsored by Peter Van Eynde <pvaneynd@debian.org>.

 -- Emanuele Rocca <emarocca@libero.it>  Tue, 15 Jul 2003 18:45:02 +0200

fortunes-it (1.51-5.1) unstable; urgency=low

  * NMU: fortunes-mod added to build-deps.
    (closes: #147018)

 -- Francesco Paolo Lovergine <frankie@debian.org>  Mon, 28 Oct 2002 10:12:15 +0100

fortunes-it (1.51-5) unstable; urgency=low

  * lintian fixes to changelog, build-depends and standard version

 -- Peter Van Eynde <pvaneynd@debian.org>  Wed, 24 Apr 2002 20:30:22 +0200

fortunes-it (1.51-4) unstable; urgency=low

  * New maintainer.
  * Went to debhelper, and FHS.

 -- Peter Van Eynde <pvaneynd@debian.org>  Thu,  7 Oct 1999 02:54:05 +0200

fortunes-it (1.51-3) unstable; urgency=low

  * Debian QA upload.
  * Placed the fortunes directly into the right directory
  * Used the right version of fortune-mod for the dependency.

 -- Torsten Landschoff <torsten@debian.org>  Sun,  8 Aug 1999 15:53:08 +0200

fortunes-it (1.51-2) unstable; urgency=low

  * Non maintainer upload.
  * Orphaned the package (as Marco said he did it in a BTS mail).
  * Changed the rules file to build the .dat files on packaging.
  * Moved the interesting stuff to binary-indep as we just write data files.
  * Changed dependency to current fortune-mod package.
  * Added provide entry for fortune-cookie-db.

 -- Torsten Landschoff <torsten@debian.org>  Sun,  8 Aug 1999 14:39:09 +0200

fortunes-it (1.51-1) unstable; urgency=low

  * Initial Release.

 -- Marco d'Itri <md@linux.it>  Thu, 12 Mar 1998 18:05:06 +0100
