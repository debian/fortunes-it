Source: fortunes-it
Section: games
Priority: optional
Build-Depends:
 debhelper (>= 13),
 debhelper-compat (= 13)
Build-Depends-Indep: fortune-mod, recode
Maintainer: Salvo 'LtWorf' Tomaselli <ltworf@debian.org>
Rules-Requires-Root: no
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/debian/fortunes-it
Vcs-Git: https://salsa.debian.org/debian/fortunes-it.git
Homepage: https://codeberg.org/fortunes-it/fortunes-it

Package: fortunes-it
Architecture: all
Depends: ${misc:Depends}, fortune-mod
Provides: fortune-cookie-db
Suggests: fortunes-it-off
Description: Data files containing Italian fortune cookies
 This package provides a collection of 'fortune cookies' in Italian, taken
 from:
  * The newsgroup it.hobby.umorismo
  * Andrea `Zuse` Balestrero's personal archive
  * Various tv transmissions, such as "Mai dire Gol" or "Striscia la Notizia"
  * The book "Jack Frusciante e` uscito dal gruppo"
  * Other various sources

Package: fortunes-it-brianza
Architecture: all
Depends: ${misc:Depends}, fortune-mod
Provides: fortune-cookie-db
Suggests: fortunes-it
Description: Data files containing Brianza fortune cookies
 This package contains proverbs from Brianza.
 .
 It was originally part of the fortunes-it package but is
 now separate because it is not in Italian.

Package: fortunes-it-off
Architecture: all
Depends: ${misc:Depends}, fortune-mod
Provides: fortune-cookie-db
Suggests: fortunes-it
Description: Data files containing Italian fortune cookies, offensive section
 This package contains a set of 'fortune cookies' in Italian, separated from
 the fortunes-it package since someone may consider they to be offensive.
 .
 Please do not install this package if you or your users are easily
 offended.

Package: fortunes-scn
Architecture: all
Depends: ${misc:Depends}, fortune-mod
Provides: fortune-cookie-db
Suggests: fortunes-scn-off
Description: Data files containing Sicilian fortune cookies
 This package provides a collection of 'fortune cookies' in Sicilian, mostly:
  * Proverbs
  * Common expressions
  * Songs
  * Literature

Package: fortunes-scn-off
Architecture: all
Depends: ${misc:Depends}, fortune-mod
Provides: fortune-cookie-db
Description: Data files containing Sicilian fortune cookies, offensive section
 This package contains a set of 'fortune cookies' in Sicilian, separated from
 the fortunes-scn package since someone may consider they to be offensive.
 .
 Please do not install this package if you or your users are easily
 offended.
